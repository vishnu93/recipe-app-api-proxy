# recipe-app-api-proxy


NGNIX proxy app for our API to serve static files as uwsgi is not optimized to serve static files(HTML,CSS,js)

## usage

### Environment Variables

* `LISTEN_PORT` -port to listen on (default:`8000`)

* `APP_HOST` - host name of the app for forward requests(default:`app`)

* `APP_PORT` -port  of the app for forward requests(default:`9000`)

